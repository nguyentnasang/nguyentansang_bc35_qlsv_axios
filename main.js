// bat loading
function batLoading() {
  document.getElementById("spinners").style.display = "flex";
}
// tat loading
function tatLoading() {
  document.getElementById("spinners").style.display = "none";
}
function resetForm() {
  document.getElementById("formQLSV").reset();
}
// lấy danh sách sinh vien service

function fetchDssvSevice() {
  batLoading();
  axios({
    url: "https://63452fb339ca915a69f7f5b4.mockapi.io/sv",
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      renderDanhSachSinhVien(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log("err", err);
    });
}
fetchDssvSevice();
function renderDanhSachSinhVien(list) {
  var contenrHTML = "";
  list.forEach(function (sv) {
    var tinhdtb = (sv.diemToan * 1 + sv.diemLy * 1 + sv.diemHoa * 1) / 3;
    contenrHTML += `<tr>
    <td>${sv.ma}</td>
    <td>${sv.ten}</td>
    <td>${sv.email}</td>
    <td>${tinhdtb.toFixed(1)}</td>
    <td>
    <button onclick="suaSv(${sv.ma})" class="btn btn-primary">sua</button>
    <button onclick="xoaSv(${sv.ma})" class="btn btn-danger">xoa</button>
    </td>
    </tr>`;
  });
  document.getElementById("tbodySinhVien").innerHTML = contenrHTML;
}
// xóa sinh viên
function xoaSv(idma) {
  batLoading();
  axios({
    url: `https://63452fb339ca915a69f7f5b4.mockapi.io/sv/${idma}`,
    method: "DELETE",
  })
    .then(function (res) {
      tatLoading();
      fetchDssvSevice();
      Swal.fire("xoa thanh cong");
    })
    .catch(function (err) {
      tatLoading();
      Swal.fire("xoa that bai");
    });
}
// thêm sinh viên
function themSv() {
  batLoading();
  var sv = layThongTinTuForm();

  axios({
    url: "https://63452fb339ca915a69f7f5b4.mockapi.io/sv",
    method: "POST",
    data: sv,
  })
    .then(function (res) {
      tatLoading();
      fetchDssvSevice();
      Swal.fire("thêmm thanh cong");
      resetForm();
    })
    .catch(function (err) {
      tatLoading();
      Swal.fire("thêmm that bại");
    });
}

// sửa sinh vien
// show thong tin len form
function suaSv(idsv) {
  batLoading();
  document.getElementById("txtMaSV").disabled = true;
  axios({
    url: `https://63452fb339ca915a69f7f5b4.mockapi.io/sv/${idsv}`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      showThongTinLenForm(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log("err", err);
    });
}
// cập nhật sinh viên
function capNhatSv() {
  batLoading();
  var sv = layThongTinTuForm();
  document.getElementById("txtMaSV").disabled = false;

  axios({
    url: `https://63452fb339ca915a69f7f5b4.mockapi.io/sv/${sv.ma}`,
    method: "PUT",
    data: sv,
  })
    .then(function (res) {
      tatLoading();
      fetchDssvSevice();
      Swal.fire("cập nhật thanh cong");

      resetForm();
    })
    .catch(function (err) {
      tatLoading();
      Swal.fire("cập nhật thất bại");
    });
}
