function layThongTinTuForm() {
  var maSv = document.getElementById("txtMaSV").value;
  var tenSv = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var matKhau = document.getElementById("txtPass").value;
  var diemToan = document.getElementById("txtDiemToan").value;
  var diemLy = document.getElementById("txtDiemLy").value;
  var diemHoa = document.getElementById("txtDiemHoa").value;
  var sv = new SinhVien(maSv, tenSv, email, matKhau, diemToan, diemLy, diemHoa);
  return sv;
}
function renderDSSV(list) {
  var contentHTML = "";
  for (var i = 0; i < list.length; i++) {
    contentHTML += `<tr>
    <td>${list[i].ma}</td>
    <td>${list[i].ten}</td>
    <td>${list[i].email}</td>
    <td>${list[i].tinhDTB()}</td>
    <td>
    <button onclick='suaSV("${
      list[i].ma
    }")' class='btn btn-primary'>sua</button>
    <button onclick='xoaSV("${list[i].ma}")' class='btn btn-danger'>xoa</button>

    </td>
    </tr>`;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}
function showThongTinLenForm(edit) {
  document.getElementById("txtMaSV").value = edit.ma;
  document.getElementById("txtTenSV").value = edit.ten;
  document.getElementById("txtEmail").value = edit.email;
  document.getElementById("txtPass").value = edit.matKhau;
  document.getElementById("txtDiemToan").value = edit.diemToan;
  document.getElementById("txtDiemLy").value = edit.diemLy;
  document.getElementById("txtDiemHoa").value = edit.diemHoa;
}
